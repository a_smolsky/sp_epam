# Task
Write a ruby script (ruby_app) that:

* a. Receives a log as argument (webserver.log is provided) e.g.: ./parser.rb webserver.log
* b. Returns the following:
    * list of webpages with most page views ordered from most pages views to less page views e.g.:
      ```
      /home 90 visits /index 80 visits etc...
      ```
    * list of webpages with most unique page views also ordered e.g.:
      ```
      /about/2 8 unique views`
      /index 5 unique views etc...
      ```
# Expected output
```
***** Most viewed pages *****
/about/2: 90 visits
/contact: 89 visits
/index: 82 visits
/about: 81 visits
/help_page/1: 80 visits
/home: 78 visits

***** Most unique page views *****
/help_page/1: 23 unique views
/contact: 23 unique views
/home: 23 unique views
/index: 23 unique views
/about/2: 22 unique views
/about: 21 unique views
```
    
# Run
* Clone and prepare to run
    * `$ git clone https://gitlab.com/a_smolsky/sp_epam.git`
    * `$ cd sp_epam`
    * `$ bundle`
    * `$ chmod +x bin/log_parser`
* Execute
    * Run `./bin/log_parser webserver.log`
    
# Tests
* Integration: `rspec --tag integration`
* Unit: `rspec --tag "~integration"`
    
# Code Analysis
* Run `rubocop` or `rubocop <path-to-file>`

# Requirements and extra capabilities
* Script allows random names of the log file.
* File name should be consisted of any non-whitespace characters and have `.log` extension

# Additional tasks that could be done in future
* Update file rows validation to collect all incorrect rows and create report file
* Review and improve unit tests
