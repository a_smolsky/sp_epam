# frozen_string_literal: true

module Lib
  module LogParser
    class Presenter
      def initialize(entries)
        @entries = entries
      end

      def print
        print_most_viewed
        puts "\n"
        print_unique_viewed
      end

      private

      attr_reader :entries

      def print_most_viewed
        puts '***** Most viewed pages *****'
        entries.all.each do |url, views|
          puts "#{url}: #{views} visits"
        end
      end

      def print_unique_viewed
        puts '***** Most unique page views *****'
        entries.unique.each do |url, visits|
          puts "#{url}: #{visits} unique views"
        end
      end
    end
  end
end
