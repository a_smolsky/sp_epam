# frozen_string_literal: true

require_relative 'entry'
require_relative 'entries'

module Lib
  module LogParser
    class Parser
      def initialize(
        file,
        entry_class: Entry,
        entries_class: Entries
      )
        @file = file
        @entry_class = entry_class
        @entries_class = entries_class
      end

      def parse
        entries_class.new(parsed_data)
      end

      private

      attr_reader :file, :entry_class, :entries_class

      def parsed_data
        file.map { |row| entry_class.new(row).tap(&:valid?) }
      end
    end
  end
end
