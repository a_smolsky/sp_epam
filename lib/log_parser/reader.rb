# frozen_string_literal: true

module Lib
  module LogParser
    class Reader
      def initialize(path)
        @path = path
      end

      def read
        File.readlines(path)
      end

      private

      attr_reader :path
    end
  end
end
