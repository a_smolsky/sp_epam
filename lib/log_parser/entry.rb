# frozen_string_literal: true

module Lib
  module LogParser
    class Entry
      attr_reader :url, :ip

      VALID_URL = %r{^\/\S+\/?\S*$}.freeze
      VALID_IP = %r{^\d{1,3}(?:\.\d{1,3}){3}$}.freeze

      def initialize(row)
        @url, @ip = row.split(' ')
      end

      def valid?
        raise(StandardError, 'Incorrect url format') unless VALID_URL =~ url
        raise(StandardError, 'Incorrect ip format') unless VALID_IP =~ ip

        true
      end
    end
  end
end
