# frozen_string_literal: true

module Lib
  module LogParser
    class Entries
      def initialize(parsed_entries)
        @entries = parsed_entries
      end

      def all
        sort(all_filter)
      end

      def unique
        sort(unique_filter)
      end

      private

      attr_reader :entries

      def sort(filtered_entries)
        filtered_entries.sort_by { |row| -row.last }
      end

      def all_filter
        mapped_entries.map { |url, ips| [url, ips.size] }
      end

      def unique_filter
        mapped_entries.map { |url, ips| [url, ips.uniq(&:ip).size] }
      end

      def mapped_entries
        @mapped_entries ||= entries.group_by(&:url)
      end
    end
  end
end
