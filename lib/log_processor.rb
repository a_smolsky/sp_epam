# frozen_string_literal: true

require_relative 'log_parser/reader'
require_relative 'log_parser/parser'
require_relative 'log_parser/presenter'

module Lib
  class LogProcessor
    def initialize(
      file_path,
      reader_class: LogParser::Reader,
      parser_class: LogParser::Parser,
      presenter_class: LogParser::Presenter
    )
      @path = file_path
      @reader = reader_class
      @parser = parser_class
      @presenter = presenter_class
    end

    def process
      print
    end

    private

    attr_reader :path, :reader, :parser, :presenter

    def read
      reader.new(path).read
    end

    def parse
      parser.new(read).parse
    end

    def print
      presenter.new(parse).print
    end
  end
end
