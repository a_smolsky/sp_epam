# frozen_string_literal: true

require 'log_processor'

describe Lib::LogProcessor do
  subject { described_class.new(file_path, reader_class: reader, parser_class: parser, presenter_class: presenter) }

  let(:file_path) { :path }
  let(:reader) do
    double(new: double(read: true))
  end
  let(:parser) do
    double(new: double(parse: true))
  end
  let(:presenter) do
    double(new: double(print: :expected_result))
  end

  it 'processes webserver log' do
    expect(subject.process).to eq(:expected_result)
  end
end
