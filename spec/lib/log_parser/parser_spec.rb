# frozen_string_literal: true

require 'log_parser/parser'

describe Lib::LogParser::Parser do
  subject { described_class.new(file_mock, entry_class: entry_mock, entries_class: entries_mock).parse }

  context '' do
    let(:file_mock) { [:file_row] }
    let(:entry_mock) { double(new: entry_instance_mock) }
    let(:entry_instance_mock) { double(tap: :mapped_file_row) }
    let(:entries_mock) { class_double('EntriesClass') }

    it 'creates log entries storage object' do
      expect(entries_mock).to receive(:new).with([:mapped_file_row]).and_return(:expected_result)
      expect(subject).to eq(:expected_result)
    end
  end
end
