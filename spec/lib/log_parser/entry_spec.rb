# frozen_string_literal: true

require 'log_parser/entry'

describe Lib::LogParser::Entry do
  subject { described_class.new(log_row) }

  let(:incorrect_url) { 'url' }
  let(:incorrect_ip) { 'ip' }
  let(:correct_url) { '/correct/url' }
  let(:correct_ip) { '111.111.111.111' }

  context 'incorrect url' do
    let(:log_row) { "#{incorrect_url} #{incorrect_ip}" }

    it 'raises url format error' do
      expect { subject.valid? }.to raise_error(StandardError, 'Incorrect url format')
    end
  end

  context 'incorrect ip' do
    let(:log_row) { "#{correct_url} #{incorrect_ip}" }

    it 'raises ip format error' do
      expect { subject.valid? }.to raise_error(StandardError, 'Incorrect ip format')
    end
  end

  context 'correct url and ip' do
    let(:log_row) { "#{correct_url} #{correct_ip}" }

    it 'creates new entry' do
      expect(subject.valid?).to be_truthy
      expect(subject.url).to eq(correct_url)
      expect(subject.ip).to eq(correct_ip)
    end
  end
end
