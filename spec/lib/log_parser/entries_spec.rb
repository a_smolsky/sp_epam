# frozen_string_literal: true

require 'log_parser/entries'

describe Lib::LogParser::Entries do
  subject { described_class.new(entries_mock) }

  let(:entries_mock) { double(group_by: grouped_entries_mock) }
  let(:grouped_entries_mock) { double(map: filtered_entries) }

  context 'all views' do
    let(:filtered_entries) { object_double('AllEntries') }
    let(:expected_output) { :all_entries }

    it 'returns all views' do
      expect(filtered_entries).to receive(:sort_by).and_return(expected_output)
      expect(subject.all).to eq(expected_output)
    end
  end

  context 'unique views' do
    let(:filtered_entries) { object_double('UniqueEntries') }
    let(:expected_output) { :unique_entries }

    it 'returns all views' do
      expect(filtered_entries).to receive(:sort_by).and_return(expected_output)
      expect(subject.unique).to eq(expected_output)
    end
  end
end
