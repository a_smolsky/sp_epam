# frozen_string_literal: true

require 'log_parser/presenter'

describe Lib::LogParser::Presenter do
  subject { described_class.new(entries_mock).print }

  let(:entries_mock) do
    double(
      all: [
        %w[test/1 100],
        %w[test/2 10]
      ],
      unique: [
        %w[test/3 101],
        %w[test/4 11]
      ]
    )
  end

  let(:expected_output) do
    <<~OUTPUT
      ***** Most viewed pages *****
      test/1: 100 visits
      test/2: 10 visits

      ***** Most unique page views *****
      test/3: 101 unique views
      test/4: 11 unique views
    OUTPUT
  end

  it 'returns expected data' do
    expect { subject }.to output(expected_output).to_stdout
  end
end
