# frozen_string_literal: true

require 'log_parser/reader'

describe Lib::LogParser::Reader do
  subject { described_class.new(path).read }

  let(:path) { :path }

  before { allow(File).to receive(:readlines).and_return(:expected_result) }

  it 'returns expected data' do
    expect(subject).to eq(:expected_result)
  end
end
