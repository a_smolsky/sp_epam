# frozen_string_literal: true

require 'open3'

describe 'bin/log_parser', integration: true do
  subject { Open3.popen3(command) }

  let(:command) { "./bin/log_parser #{arguments}" }

  context 'no arguments' do
    let(:arguments) { '' }

    it 'raises no options error' do
      expect(subject[1].read).to be_empty
      expect(subject[2].read).to include('No options')
    end
  end

  context 'wrong arguments' do
    let(:arguments) { 'wrong_name' }

    it 'raises incorrect file name error' do
      expect(subject[1].read).to be_empty
      expect(subject[2].read).to include('Incorrect file name')
    end
  end

  context 'file not exist' do
    let(:arguments) { 'nonexistent.log' }

    it 'raises file not found error' do
      expect(subject[1].read).to be_empty
      expect(subject[2].read).to include('File not found')
    end
  end

  context 'correct arguments' do
    let(:arguments) { 'spec/fixtures/webserver.log' }
    let(:expected_output) do
      <<~OUTPUT
        ***** Most viewed pages *****
        /test/2: 3 visits
        /test/1: 2 visits

        ***** Most unique page views *****
        /test/1: 2 unique views
        /test/2: 1 unique views
      OUTPUT
    end

    it 'processes log file' do
      expect(subject[1].read).to include(expected_output)
      expect(subject[2].read).to be_empty
    end
  end
end
